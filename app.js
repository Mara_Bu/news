const selectCountry = document.querySelector('.country');
const selectCategory = document.querySelector('.category');

const service = new NewsService();
const ui = new NewsUI();

selectCountry.addEventListener('change', selectionChanged);
selectCategory.addEventListener('change', selectionChanged);

function selectionChanged() {
    let mainDiv = document.querySelector('.main-div');
    mainDiv.innerHTML = "";
    getNews(selectCountry.value, selectCategory.value);   
}

function getNews(country, category) {
    service.getData(country, category)
        .then((res) => { 
            ui.renderAllNews(res.articles)
        });
}

