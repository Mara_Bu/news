class NewsUI {
    constructor() {
        this.articles = [];
        this.urlToImage = "";
        this.title = "";
        this.description = "";
        this.url = "";
    }

    renderAllNews(arrayNews) {
        this.articles = arrayNews
        this.articles.forEach((news) => {
            let node = this.renderOneNews(news)
            let mainDiv = document.querySelector('.main-div')
            mainDiv.appendChild(node);  
        });
    }

    renderOneNews(news) {
        console.log(news.title, news.content, news.source.name);   
        let cardDiv = document.createElement('div');
        cardDiv.classList.add('news-card')
        cardDiv.innerHTML = `
            <img src="${news.urlToImage}" alt="img">
            <div class="news-card-content">
                <h3 class="title">${news.title}</h3>
                <p>${news.description || ''}</p>
                <div class="grey-line"></div>
                <a href="${news.url}" class="read-more">Read more</a>
                </div>
            `;
        return cardDiv;
    }
}
