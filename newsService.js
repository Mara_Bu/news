class NewsService {
    constructor() {
        this.apiUrl = 'https://newsapi.org/v2/top-headlines?';
        this.apiKey = '9c27b0f722b84da5a08312d2b125351b';
        this.country = 'ua';
        this.category = 'general';
    }

    getData(country = this.country, category= this.category) {
        let url = this.apiUrl + 'country=' + country + '&apiKey=' + this.apiKey + '&category=' + category; 

        return fetch(url)
            .then((response) =>  response.json())
            .catch((error) => {console.error(error)})
    }
}